# TEST Cobrando BPO!

Este es el backend de la prueba para el proceso de Selección de cobrando BPO

hice todo lo que pidieron y pues ademas le agrege dos cosas por que pues o si no queda muy debil la aplicación, primero un metodo de autentificación por token y segundo el crud tambien de los departamentos pa poder que puedan probar adecuadamente el proyecto

## Settings

Usa las variables y constantes de configuración, detalladas en la documentación: [configuraciones](http://cookiecutter-django.readthedocs.io/en/latest/settings.html).


## Docker

El proyecto esta fuertemente cimentado en el uso de Docker, y docker-compose, tan para los entornos de desarrollo, como producción, pero como es una prueba solo ejecute el local, el produccion se cambiaria si se necesitara pero pues no es necesario por el momento. Para mas detalles, verificar los comandos en [cookiecutter-django Docker documentation](http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html).

nota: si no quiere usar docker, prenda un entorno e instale los requerimientos y lo corres normal

## Comandos basicos de trabajo

### Configurando usarios

-   Normalmente pues para que el microservicio sea seguro se debe tener una parte de autentificación, entonces lo agrego pero se que aveces es otro microservicio el que solo se encarga de autentificar pero pues lo dejo asi como por el tiempo; Para crear una cuenta de **usuario normal**, se debe registrar por medio de la url register dando los datos del usuario y ya luego pues autorizas las peticiones que necesites, el swagger ayuda mucho, te recomiendo verlo cuando lo vaya a consumir algun front

## Ponerlo a correr
    $ sudo docker-compose -f local.yml build
    $ sudo docker-compose -f local.yml up

## Autentificar rutas
    $ registrese, luego has login y le da un token
    $ le da en el candado escribe "Bearer {token}"

    
### Migraciones

    $ docker-compose -f local.yml run --rm django python manage.py makemigrations
    $ docker-compose -f local.yml run --rm django python manage.py migrate

### Validación de tipado

Running type checks with mypy:

    $ docker-compose -f local.yml run --rm django mypy microserve_staff

### Análisis de código estatico

El proyecto incluye Flake8, Bandit, y PyLint, para los dos primeros, solo es correr el proceso directamente en el archivo del proyecto. Para el caso de PyLint, si se hace necesaria la configuración de las variables del proyecto, por lo que se corre directamente en Docker.

    $ docker-compose -f local.yml run --rm django pylint microserve_staff

### Cobertura de pruebas

Para correr las pruebas, es importante usar la cobertura de los mismos, lo que ademas permite visualizar en resultado en un reporte HTML:

    $ docker-compose -f local.yml run --rm django coverage run -m pytest
    $ docker-compose -f local.yml run --rm django coverage html
    $ open htmlcov/index.html

pero para la prueba no se pide entonces no  hay, no lo corras, solo te dejo documentado como hacerlo despues

#### Correr los test usando pytest

    $ docker-compose -f local.yml run --rm django pytest


## Despliegue a producción

Para mayor información sobre el despliegue del proyecto en entornos de producción, revisar la documentación [usando Docker](https://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html)


## BASE DE DATOS

el proyecto esta configurado para que funcione con postgrests, en el .env pon las credenciales de tu bd o si quieres levanta un docker con una bd de postgrest y pues igual das las credenciales, no lo pongo en el mismo contenedor por que no es buena practica, pero podes encontrar información de como levantarlo con docker aca: https://www.docker.com/blog/how-to-use-the-postgres-docker-official-image/