from django.db import models


class Department(models.Model):
    code = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    budget = models.FloatField(blank=True,null=True)

    class Meta:
        verbose_name = "Departament"
        verbose_name_plural = "Departaments"


class Employee(models.Model):
    code = models.IntegerField(primary_key=True)
    nit = models.CharField(max_length=9, blank=True, null=True)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    first_surname = models.CharField(max_length=100, blank=True, null=True)
    second_surname = models.CharField(max_length=100, blank=True, null=True)
    code_departament = models.ForeignKey(Department, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Employee"
        verbose_name_plural = "Employees"
