from rest_framework.routers import DefaultRouter

from microserve_staff.staff.api.viewsets.departamentViewset import DepartamentViewSet
from microserve_staff.staff.api.viewsets.employeeviewset import EmployeeViewSet


router = DefaultRouter()

router.register("departament", DepartamentViewSet, basename="order")
router.register("employee", EmployeeViewSet, basename="staff")

urlpatterns = router.urls
