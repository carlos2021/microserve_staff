from rest_framework import serializers
from microserve_staff.staff.models import Department


class DepartamentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = "__all__"
