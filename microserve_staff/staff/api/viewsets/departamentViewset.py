from rest_framework import viewsets
from microserve_staff.staff.models import Department
from microserve_staff.staff.api.serializers.departmentSerializer import (
    DepartamentSerializer,
)


class DepartamentViewSet(viewsets.ModelViewSet):
    model = Department
    serializer_class = DepartamentSerializer
    queryset = Department.objects.all()
    http_method_names = ["get", "post"]