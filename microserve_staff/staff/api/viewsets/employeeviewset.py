from rest_framework import viewsets
from microserve_staff.staff.models import Employee
from microserve_staff.staff.api.serializers.employeeSerializer import EmployeeSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    model = Employee
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()
