from django.contrib.auth import authenticate

from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from django.http import JsonResponse

from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenObtainPairView

from microserve_staff.users.api.serializers import (
    CustomTokenObtainPairSerializer,
    CustomUserSerializer,
    RegisterSerializer,
)
from microserve_staff.users.models import User
from rest_framework import permissions
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import action


class Login(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        email = request.data["email"]
        password = request.data["password"]
        user = authenticate(email=email, password=password)

        if user:
            login_serializer = self.serializer_class(data=request.data)
            if login_serializer.is_valid():
                user_serializer = CustomUserSerializer(user)
                return Response(
                    {
                        "token": login_serializer.validated_data.get("access"),
                        "refresh_token": login_serializer.validated_data.get("refresh"),
                        "user": user_serializer.data,
                        "staff": "Inicio de Sesion Existoso",
                    },
                    status=status.HTTP_200_OK,
                )
            return Response(
                {"error": "Contraseña o email incorrectos"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return Response(
            {"error": "Contraseña o nombre de usuario incorrectos"},
            status=status.HTTP_400_BAD_REQUEST,
        )


class Logout(GenericAPIView):
    def post(self, request, *args, **kwargs):
        user = User.objects.filter(id=request.data.get("user", 0))
        if user.exists():
            RefreshToken.for_user(user.first())
            return Response(
                {"staff": "Sesión cerrada correctamente."}, status=status.HTTP_200_OK
            )
        return Response(
            {"error": "No existe este usuario."}, status=status.HTTP_400_BAD_REQUEST
        )


class RegisterApi(GenericAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = RegisterSerializer

    def post(self, request) -> Response:
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return JsonResponse({"serializer": "created"}, status=status.HTTP_201_CREATED)
