from django.urls import path
from django.urls import re_path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.shortcuts import redirect


schema_view = get_schema_view(
    openapi.Info(
        title="STAFF Back API",
        default_version="v0.01",
        description="microserve_staff api-rest",
        terms_of_service="nothing",
        contact=openapi.Contact(email="5carlosmoreno2021@gmail.com"),
        license=openapi.License(name="open source"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
    
)



urlpatternsSwagger = [
    re_path(
        r"swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
    path("", lambda request: redirect("swagger/", permanent=False)),
]
